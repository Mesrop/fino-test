import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';

@Injectable()
export class RequestsService {

  constructor(private http: Http,) { }


  pat: string = 'https://api.themoviedb.org/3';
  directory: string = '/discover';
  type: string = '/movie?';
  apiKey: string = '451a5c46225283a9a3e766eee8fa80ac';
  language: string = 'en-US';
  sort: string = 'popularity.desc';
  adult: string = 'false';
  video: string = 'false';
  pagination: string = '1';
  api: string = `${this.pat}${this.directory}${this.type}api_key=${this.apiKey}&language=${this.language}&sort_by=${this.sort}&include_adult=${this.adult}&include_video=${this.video}&page=${this.pagination}`;


  getMovies() {
    return this.http.get(this.api)
        .map(data => data.json())
  }

  getSearchItems(val: string) {
    return this.http.get(`${this.pat}/search${this.type}api_key=${this.apiKey}&language=${this.language}&query=${val}&page=${this.pagination}&include_adult=${this.adult}`)
        .map((res: Response) => res.json())
  }
}
