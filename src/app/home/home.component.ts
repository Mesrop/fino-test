import {Component, OnInit, Input, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {RequestsService} from '../services/requests.service';
import 'rxjs/add/operator/map';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
@Injectable()
export class HomeComponent implements OnInit {

    public allMovies;
    public page;

    @Input()
    openSearch: boolean = false;

    constructor(private http: Http, private requests: RequestsService) {
        
    };


    ngOnInit() {
        this.getMovies();
    }

    getWatchList(){
        this.allMovies = JSON.parse(localStorage.getItem('watchLater'));
        this.allMovies = this.allMovies.reverse()
    }


    getMoviesItem(e){
        let localObj = JSON.parse(localStorage.getItem('watchLater'));


        if(localObj){
            let exMovie = localObj.find(obj => {
                return obj.id === e.id;
            });

            if (exMovie) {
                for(let i = 0; i < localObj.length; i ++){
                    if(localObj[i].id == exMovie.id){
                        localObj.splice(i,1);
                        localStorage.setItem('watchLater', JSON.stringify(localObj));
                    }
                }
            } else{
                localObj.push(e);
                localStorage.setItem('watchLater', JSON.stringify(localObj));
            }
        } else{
            let localItems = [];
            localItems.push(e);
            localStorage.setItem('watchLater', JSON.stringify(localItems));
        }

    }

    checkWatchList(id){
        let localObj = localStorage.getItem('watchLater');
        if(localObj){
            return localObj.indexOf(id.toString()) > -1
        }
    }

    getMovies() {
        this.requests.getMovies()
        .subscribe(
                data => {
                    this.allMovies = data.results;
                    this.page = data.page;
                },
                err => console.error(err)
            );
        }

    getSearchItems(val: string) {
        if(val == ''){
            this.getMovies();
        } else{
            this.requests.getSearchItems(val)
                .subscribe(
                    data => {
                        this.allMovies = data.results;
                        this.page = data.page;
                    },
                    err => console.error(err)
                );

        }
    }

}
