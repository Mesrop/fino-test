import { Component,  Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})


export class SidebarComponent{

  @Output()
  emitEvent = new EventEmitter();
  @Output()
  getWatchLater = new EventEmitter();
  @Output()
  getPopularLater = new EventEmitter();


  @Input()
  openSearch:boolean = false;

  constructor() {

  };

  getWatchList(){
    this.getWatchLater.next(null)
  }
  getPopularList(){
    this.getPopularLater.next(null)
  }

  isActiveSearch(e){
    if(e !== undefined){
      this.emitEvent.next(this.openSearch = e);
    }else{
      this.emitEvent.next(this.openSearch = !this.openSearch);
    }
  }


}