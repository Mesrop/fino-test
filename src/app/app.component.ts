import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

	@ViewChild('sidebar') sidebar;
	@ViewChild('home') home;

	constructor() {

	}

	sidebarMessage(e){
		this.home.openSearch = e;


	}
	watchLaterSvc(){
		this.home.getWatchList();
	}

	popularLaterSvc(){
		this.home.getMovies();
	}

	isActiveSearch(){
		this.sidebar.isActiveSearch(false);
	}

}
