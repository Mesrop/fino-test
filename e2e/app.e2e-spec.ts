import { KinoPage } from './app.po';

describe('kino App', () => {
  let page: KinoPage;

  beforeEach(() => {
    page = new KinoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
